#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const int LARGO_NOMBRE = 200;

static const int ORDEN_ID = 1;
static const int ORDEN_NOMBRE = 2;

const int TRABAJADOR = 1;
const int PROFESOR_PLANTA = 2;
const int PROFESOR_HONORARIOS = 3;

const int SUELDO_FIJO = 1;
const int SUELDO_VARIABLE = 2;

const int NORMAL_SEARCH = 0;
const int BINARY_SEARCH = 1;
const int DEFAULT_SEARCH = 1;

char* enum_tipo_trabajador[] = { "Trabajador", "Profesor Planta", "Profesor Honorarios", NULL };

char* enum_sino[] = { "Si", "No", NULL };

char* enum_asignaturas[] = {
    "Asignatura 01 - Seccion 50",
    "Asignatura 02 - Seccion 50",
    "Asignatura 03 - Seccion 50",
    "Asignatura 04 - Seccion 50",
    "Asignatura 05 - Seccion 50",
    "Asignatura 06 - Seccion 50",
    NULL
};

char* enum_asignaturas_especificas[] = {
    "Asignatura Especifica 01 - Session 50",
    "Asignatura Especifica 02 - Seccion 50",
    "Asignatura Especifica 03 - Seccion 50",
    "Asignatura Especifica 04 - Seccion 50",
    "Asignatura Especifica 05 - Seccion 50",
    "Asignatura Especifica 06 - Seccion 50",
    NULL
};

char* enum_area_carrera[] = {
    "Area Cyberseguridad",
    "Area Informatica",
    "Area Matematicas",
    "Area Programacion",
    NULL
};

const char* enum_sexo[] = {
    "FEMENINO",
    "MASCULINO",
    NULL};

// Definicion del Tipo Trabajador (T por tipo o type)
typedef struct Trabajador TTrabajador;
struct Trabajador {
    int tipoTrabajador; // 1=trabajador, 2=profesor planta, 3=profesor honorarios,

    // Campos comunes normalizados
    int id;
    char* nombre_completo;
    int sueldo; // Es fijo, excepto cuando es profesor honorarios que es variable
        
    int cantidad_asignaturas;

    // Arreglo: Cuando tipoTrabajador=2 contiene asignaturas, cuando tipoTrabajador=2 contiene asignaturas especificas
    int* id_asignaturas;

    // Campos para profesores de planta
    int area_carrera;

    // Campos para profesores a honorarios
    int cantidad_horas;
    int valor_hora;

    // Otros campos propios de los trabajadores
    int sexo;
    
};

// DEfinicion del tipo TNodoLista (T, por tipo o type)
typedef struct struct_nodo_lista TNodoLista;
struct struct_nodo_lista {
    TTrabajador* pValor;
    TNodoLista *prox;
};

typedef struct base {
    TNodoLista* plista;
    int correlativo; // Numero correlativo unico global para el aplicativo
} TBaseDatos;

// Operaciones con el trabajador
TTrabajador* trabajador_nuevo(int id, char* nombre, int tipo, int sueldo);
TTrabajador* trabajador_copiar(TTrabajador*);
int trabajador_comparar(TTrabajador* t1, TTrabajador* t2, int campo); // Compara dos trabajadores, 1 por ID, 2 por nombre (t1->id < t2->id)
TTrabajador* trabajador_ingresar(TBaseDatos*);
void trabajador_mostrar(TTrabajador*);
void trabajador_mostrar_linea(TTrabajador*);



// Operaciones sobre la lista
TNodoLista *lista_agregar( TNodoLista*, TTrabajador* );
TNodoLista *lista_eliminar( TNodoLista*, TTrabajador*  );
TNodoLista* lista_buscar(TNodoLista*, int, int algoritmo); // Busqueda de un nodo en la lista por id
void lista_mostrar( TNodoLista* );
void lista_mostrar_lineas(TNodoLista*);

// Operaciones de ingreso de datos
int menu_despliegue(void);
int ingreso_trabajadores(TBaseDatos*);
int ingresar_opcion(const char** lista);
void reiniciarPantalla(void);

int ingresar_entero_positivo(char* mensaje, int *returnValue);

void profesores_por_asignatura(const char** enum_asignaturas, int tipoTrabajador, TNodoLista* lista);

void quickSort(TNodoLista** headRef, int campo_orden);
TNodoLista* binarySearch(TNodoLista* head, int id);

int* ingresar_asignaturas(const char** opciones, int* pCantidad);

void pausa(void);

// Programa principal, despliega el menu y ejecuta las opciones seleccionadas
int main()
{
    TBaseDatos basedatos;
    
    basedatos.correlativo=0;
    basedatos.plista = NULL;
    
    int opc=0;
    
    while(opc!=7) {
        
        opc=menu_despliegue();
        
        if(opc==1){
            ingreso_trabajadores(&basedatos);
            pausa();
        }
        
        if(opc==7){//Finalizar
            reiniciarPantalla();
        }
        if(basedatos.plista!=NULL) {
            if(opc==2){ //Eliminar trabajador
                // Obtene el id del trabajador a buscar
                int id = 0;
                ingresar_entero_positivo("Ingrese el ID del trabajador a eliminar\n", &id);
                
                // Ordena la base de datos por ID con QuickSort, pasando la lista por referencia
                quickSort(&basedatos.plista, ORDEN_ID);
                
                TNodoLista* nodo;
                
                nodo=lista_buscar(basedatos.plista,id,DEFAULT_SEARCH);
                
                // El trabajador a eliminar
                if(nodo!=NULL)
                {
                    int opc = 0;
                    printf("El siguiente trabajador sera eliminado, por favor confirmar:\n");
                    trabajador_mostrar_linea(nodo->pValor);
                    
                    opc=ingresar_opcion((const char**)enum_sino);
                    
                    if(opc==1) {
                        lista_eliminar(basedatos.plista,nodo->pValor);
                    }
                }
                else
                {
                    printf("Trabajador no encontrado.\n");
                }
                
                pausa();
            
            }
            if(opc==3){ //Mostrar datos de un trabajador
                
                // Obtene el id del trabajador a mostrar
                int id = 0;
                ingresar_entero_positivo("Ingrese el ID del trabajador a buscar\n", &id);
                
                // Ordena la base de datos por ID con QuickSort, pasando la lista por referencia
                quickSort(&basedatos.plista, ORDEN_ID);
                
                TNodoLista* nodo_encontrado = NULL;
                
                nodo_encontrado=lista_buscar(basedatos.plista,id,DEFAULT_SEARCH);
                
                // Muestra la lista de trabajadores
                if(nodo_encontrado!=NULL)
                {
                    trabajador_mostrar(nodo_encontrado->pValor);
                }
                else
                {
                    printf("Trabajador no encontrado.\n");
                }
        
                pausa();
                
            }
            if(opc==4){//Mostrar lista ordenada por id
                // Ordena la base de datos por Nombre con QuickSort, pasando la lista por referencia
                quickSort(&basedatos.plista, ORDEN_ID);
                // Muestra la lista de trabajadores
                lista_mostrar_lineas(basedatos.plista);
                pausa();
            }
            if(opc==5){//Mostrar lista ordenada por nombre
                // Ordena la base de datos por Nombre con QuickSort, pasando la lista por referencia
                quickSort(&basedatos.plista, ORDEN_NOMBRE);
                // Muestra la lista de trabajadores
                lista_mostrar_lineas(basedatos.plista);
                pausa();
            }
            if(opc==6){//Mostrar profesores por asignatura
                profesores_por_asignatura((const char**)enum_asignaturas, PROFESOR_PLANTA, basedatos.plista);
                profesores_por_asignatura((const char**)enum_asignaturas_especificas, PROFESOR_HONORARIOS, basedatos.plista);
                pausa();
            }
            
        }
    }
    
    return 0;
}  /*  Fin de main()  */


/* FUNCIONES   */

int ingresar_entero_positivo(char* mensaje, int *returnValue) {
    int result=0;
    int val=0;
    char buf[80];
    
    while(!result) {
        
        printf("%s", mensaje);
        
        if (fgets(buf, sizeof buf, stdin) == NULL) {
            return EOF;
        }
        
        // sscanf es más simple, pero strtol tiene un desbordamiento bien definido funcionalmente
        char sentinel;  // Lugar para almacenar basura final
        result = (sscanf(buf, "%d %c", &val, &sentinel) == 1);
        
        if(result && result>=0) {
            *returnValue = val;
        }
        else
        {
            printf("Formato de entrada incorrecto, intente nuevamente.\n");
        }
    }
    
    return result;
    
}

/*
 Permite ingresar una linea de caracteres
 */
int ingresar_caracteres(char* mensaje, int largo,char *returnValue) {
    
    printf("%s",mensaje);
    
    if (fgets(returnValue, largo, stdin) == NULL) {
        return EOF;
    }
    
    return 1;
    
}

/*
 Permite ingresar un caracter
 */
char ingresar_caracter(char* mensaje) {
    int largo = 2;
    char buffer[2];
    printf("%s",mensaje);
    if (fgets(buffer, largo, stdin) == NULL) return EOF;
    return *buffer;
}

// Despliega una lista de arreglo de punteros a caracteres, equivale a recorrer un arreglo de char*
int desplegar_lista(const char** argv) {
    int i=0;
    while ( *argv ) {
        printf( "%d-%s\n", ++i, (char*) (*argv++) );
    }
    return i;
}

// Permite elegia una opcion a partir de un arreglo de punteros a caracteres char*[] equivale a char**
int ingresar_opcion(const char** lista) {
    int opc=0;
    
    int cantidad_items=desplegar_lista(lista);
    
    printf("Por favor seleccione (de 1 a %d):\n",cantidad_items);
    
    while(opc==0) {
        
        ingresar_entero_positivo("",&opc);
        
        if(opc<1 || opc>cantidad_items) {
            opc=0;
            printf("Opcion incorrecta, intente nuevamente\n");
        }
    }
    return opc;
}

// Crea un nuevo trabajador inicializando todos sus datos
TTrabajador* trabajador_nuevo(int id, char* nombre, int tipo,int sueldo) {
    TTrabajador* ptrabajador = (TTrabajador*)malloc(sizeof(TTrabajador));
    ptrabajador->id=id;
    ptrabajador->nombre_completo =(char*) malloc(LARGO_NOMBRE+1);
    ptrabajador->tipoTrabajador = tipo;
    ptrabajador->sueldo = sueldo;
    
    // Copia el arreglo de caracteres del nombre
    strcpy(ptrabajador->nombre_completo,nombre);
    
    return ptrabajador;
}

// Destruye la memoria asignada a un trabajador
void trabajador_liberar(TTrabajador* pValue) {
    // Libera toda la memoria asignada a un trabajador
}

int trabajador_comparar(TTrabajador* t1, TTrabajador* t2,int campo) {
    if(campo==ORDEN_ID) {
        return t1->id < t2->id;
    } else {
        return strcmp((const char*) t1->nombre_completo ,(const char*) t2->nombre_completo) < 0;
    }
}

void reiniciarPantalla() {
    system("cls"); //Limpia la pantalla
    printf("Limpiando pantalla....");
    system("pause"); // Realiza una Pausa
    system("cls"); //Limpia la pantalla
}

void pausa() {
	char c=ingresar_caracter("Precione enter para continuar\n");
}

int ingreso_trabajadores(TBaseDatos* pBaseDatos) {
    TTrabajador* nu  = NULL;
    TNodoLista *plista = pBaseDatos->plista;
    int opc = 0;

    do {
        printf("\nIngrese un trabajador -->\n");
        nu = trabajador_ingresar(pBaseDatos);
        if (nu==NULL) {
            opc=2;
            continue;
        } else {
            plista= lista_agregar(plista,nu);
        }
        
        printf("Desea realizar el ingreso del siguiente trabajador?\n");
        opc = ingresar_opcion((const char**)enum_sino);

    } while (opc!=2);
    
    printf("RESUMEN DATOS INGRESADOS.");
    lista_mostrar_lineas(plista);
    
    pBaseDatos->plista = plista;
    
    return 0;
}

// Retorna 1 cuando se ingresa un trabajador valido.
TTrabajador* trabajador_ingresar(TBaseDatos* pBaseDatos) {
    
    int id=0;
    char* nombre = (char*) malloc(LARGO_NOMBRE+1);
    int tipo = 0;
    int sueldo = 0;
    int cantidad_horas = 0;
    int valor_hora = 0;
    int area_carrera = 0;
    int cantidad_asignaturas = 0;
    int sexo = 0;
    int* asignaturas = NULL;
        
    // INICIO: Ingresa todos los datos del trabajador en variables
    
    // Ingreso del tipo de trabajador
    printf("Por favor ingrese el tipo de trabajador\n");
    tipo = ingresar_opcion((const char**)enum_tipo_trabajador);
    
    ingresar_caracteres("Nombre del Trabajador:\n",LARGO_NOMBRE,nombre);
    
    printf("Seleccione sexo:\n");
    sexo = ingresar_opcion((const char**)enum_sexo);
    
    // ...resto de los campos
    if(tipo==TRABAJADOR) {
        ingresar_entero_positivo("Ingrese sueldo trabajador:\n", &sueldo);
    } else if (tipo==PROFESOR_PLANTA) {
        // Datos propios de un profesor de planta
        printf("Ingreso de datos de PROFESOR PLANTA\n");

        ingresar_entero_positivo("Ingrese sueldo fijo profesor planta:\n", &sueldo);

        printf("Por favor seleccione area/carrera del profesor\n");
        area_carrera = ingresar_opcion((const char**)enum_area_carrera);
        
        printf("Por favor indique las asignaturas del profesor.\n");
        asignaturas= ingresar_asignaturas((const char**)enum_asignaturas, &cantidad_asignaturas);
    } else if (tipo==PROFESOR_HONORARIOS)
    {
        
        // Datos propios de un profesor HONORARIOS
        printf("Ingreso de datos de PROFESOR HONORARIOS\n");

        ingresar_entero_positivo("Cantidad horas trabajadas profesor honorarios:\n", &cantidad_horas);
        ingresar_entero_positivo("Valor hora profesor honorarios:\n", &valor_hora);
        
        sueldo = cantidad_horas * valor_hora;
        
        printf("Por favor indique las asignaturas ESPECIFICAS del profesor.\n");
        asignaturas= ingresar_asignaturas((const char**)enum_asignaturas_especificas, &cantidad_asignaturas);

    }
    
    // FIN: Ingreso de datos
    
    // Poblamiento de la estructura TTrabajador con los datos ingresados
    
    // Llama a la funcion nuevo_trabajador para retornar un trabajador
    id=++pBaseDatos->correlativo;
    
    TTrabajador* pRetorno = trabajador_nuevo(id,(char*)nombre,tipo,sueldo);
    
    pRetorno->area_carrera = area_carrera;
    pRetorno->cantidad_horas = cantidad_horas;
    pRetorno->valor_hora = valor_hora;
    pRetorno->sexo = sexo;
    
    if(cantidad_asignaturas>0)
    {
        pRetorno->cantidad_asignaturas=cantidad_asignaturas;
        pRetorno->id_asignaturas = asignaturas;
    }
    
    // Retorna un puntero a un registro de trabajador con todos sus datos correspondientes
    return pRetorno;
};

// Busqueda sobre lista enlazada. Algoritmo = 0 secuencial, 1=binary search
TNodoLista* lista_buscar(TNodoLista* lista, int id, int algoritmo) {
    TNodoLista* p = lista;
    
    if(algoritmo==0) {
        while( p ) {
            
            if(p->pValor) {
                if(p->pValor->id==id) {
                    return p;
                }
            }
            p= p->prox;
        }
    } else {
        p = binarySearch(p, id);
    }
    return p;
};

// Despliega los datos de un trabajador en pantalla
void trabajador_mostrar(TTrabajador *pValor) {
    int i=0;
    printf("TRABAJADOR ID Nr#:%d", pValor->id);
    printf("NOMBRE         :%s\n", pValor->nombre_completo);
    // El sueldo desplegado depende del tipo de trabajador

    printf("SEXO           :%d (1-FEMENINO,2-MASCULINO)\n", pValor->sexo);
    
    printf("TIPO TRABAJADOR:%d-%s\n", pValor->tipoTrabajador, *(enum_tipo_trabajador+pValor->tipoTrabajador-1) );
    if(pValor->tipoTrabajador==TRABAJADOR) {
        // Desplegar campos exclusivos de un trabajador
        printf("SUELDO FIJO SEGUN CARGO:%d\n", pValor->sueldo);
    } else if (pValor->tipoTrabajador==PROFESOR_PLANTA) {
        // Desplegar campos exclusivos de un profesor planta
        printf("AREA/CARRERA           :%d-%s\n", pValor->area_carrera, *(enum_area_carrera+pValor->area_carrera-1) );
        printf("SUELDO FIJO PLANTA     :%d\n", pValor->sueldo);
        printf("=======ASIGNATURAS========");
        printf("ASIGNATURAS CANTIDAD   :%d\n", pValor->cantidad_asignaturas);
        for(i=0;i<pValor->cantidad_asignaturas;i++)
        {
            int asignatura = *(pValor->id_asignaturas+i);
            printf("ASIGNATURA ESPECIFICA:%d-%s\n", asignatura, *(enum_asignaturas_especificas+asignatura-1) );
        }
    } else if (pValor->tipoTrabajador==PROFESOR_HONORARIOS) {
        // Desplegar campos exclusivos de un profesor planta
        printf("CANTIDAD DE HORAS     :%d\n", pValor->cantidad_horas);
        printf("VALOR HORA            :%d\n", pValor->valor_hora);
        printf("SUELDO VARIABLE HONORARIOS     :%d\n", pValor->sueldo);
        printf("=======ASIGNATURAS ESPECIFICAS========");
        printf("ASIGNATURAS ESPECIFICAS CANTIDAD   :%d\n", pValor->cantidad_asignaturas);
        for(i=0;i<pValor->cantidad_asignaturas;i++)
        {
            int asignatura = *(pValor->id_asignaturas+i);
            printf("ASIGNATURA ESPECIFICA:%d-%s\n", asignatura, *(enum_asignaturas_especificas+asignatura-1) );
        }
    }}

// Despliega los datos de un trabajador en pantalla en una linea
void trabajador_mostrar_linea(TTrabajador* pValor) {
    printf("id:%d", pValor->id);
    printf("\ttipo:%d-%s", pValor->tipoTrabajador, *(enum_tipo_trabajador+pValor->tipoTrabajador-1) );
    printf("\tnombre:%s", pValor->nombre_completo);
    printf("\n");
}

/* Función que añada un datos a la LISTA */
TNodoLista *lista_agregar(TNodoLista *p, TTrabajador* n)
{
    TNodoLista *q, *r, *ant;
    
    q=(TNodoLista *)malloc(sizeof(TNodoLista));
    if (!q) {
        printf("\nError! Insuficiente espacio de memoria.");
        return(p);
    }
    
    q->pValor= n;
    ant= NULL;
    r= p;
    
    while ( r && r->pValor->id < q->pValor->id ) {
        ant= r;
        r= r->prox;
    }
    
    if ( ant == NULL ) {
        q->prox= p;
        p= q;
    }
    else {
        q->prox= r;
        ant->prox= q;
    }
    
    return(p);
}

/* Función que elimina un datos de la LISTA */
TNodoLista  *lista_eliminar(TNodoLista *p, TTrabajador* n)
{
    TNodoLista *q, *r;
    if ( p == NULL ) {
        printf("\nLista Vacia. ");
        return(p);
    }
    else {
        r= p;
        if ( p->pValor->id == n->id ) {
            p= p->prox;
            free(r);
        }
        else { /* Empieza busqueda */
            
            do {
                q= r;
                r= r->prox;
            } while ( r && r->pValor->id !=n->id );
            if (r) {
                q->prox= r->prox;
                free(r);
            }
            else
                printf("\nElemento no encontrado");
        }
    }
    
    return(p);
}

void  lista_mostrar( TNodoLista *p )
{
    printf("\n\nContenido de la lista:");
    while( p ) {
        
        trabajador_mostrar(p->pValor);
        
        p= p->prox;
    }
}

void  lista_mostrar_lineas( TNodoLista* p )
{
    printf("\n\nContenido de la lista:\n");
    while( p ) {
        
        trabajador_mostrar_linea(p->pValor);
        
        p= p->prox;
    }
}

// Despliega el menu y solicita seleccionar una opcion
int menu_despliegue() {
    system("cls");
    
    int opc = 0;
    
    printf("**** GESTION PERSONAL DOCENTE CIISA ****\n");
    
    while(opc==0) {
        printf("------------ MENU  -------------\n");
        printf("1-Ingresar un trabajador.\n");
        printf("2-Eliminar un trabajador.\n");
        printf("3-Mostrar datos trabajador.\n");
        printf("4-Mostrar lista de trabajadores ordenada por ID.\n");
        printf("5-Mostrar lista de trabajadores ordenada por NOMBRE.\n");
        printf("6-Mostrar profesores por asignatura.\n");
        printf("7-Finalizar.\n");
        
        ingresar_entero_positivo("Ingrese el numero de la opcion seleccionada\n",&opc);
        
        printf("Valor Ingresado:%d\n",opc);
        
        if(opc<1 || opc>7) {
            opc=0;
            printf("Opcion incorrecta, intente nuevamente.\n");
            pausa();
        }
    }
    
    return opc;
}

// *** FUNCIONES PARA HACER QUICKSORT DE LA LISTA POR NOMBRE o POR ID ***

/* Una función de utilidad para insertar un nodo al comienzo de la lista vinculada aprobada por referencia */
void lista_push(TNodoLista** pNodoLista, TTrabajador* pNuevoValor)
{
    TNodoLista* pNuevoNodo = (TNodoLista*) malloc(sizeof(TNodoLista));
    
    /* poner en los datos */
    pNuevoNodo->pValor = pNuevoValor;
    
    /* vincular la lista anterior del nuevo nodo */
    pNuevoNodo->prox = (*pNodoLista);
    
    /* mueve la cabeza para apuntar al nuevo nodo */
    (*pNodoLista) = pNuevoNodo;
}

// Devuelve el último nodo de la lista.
TNodoLista* getTail(TNodoLista* cur)
{
    while (cur != NULL && cur->prox != NULL)
        cur = cur->prox;
    return cur;
}

// Particiona la lista tomando el último elemento como pivote
TNodoLista* partition(TNodoLista* head, TNodoLista* end,
                      TNodoLista** newHead, TNodoLista** newEnd, int campo_orden)
{
    TNodoLista* pivot = end;
    TNodoLista* prev= NULL;
    TNodoLista* cur = head;
    TNodoLista* tail = pivot;
    
    // Durante la partición, tanto el encabezado como el final de la lista pueden cambiar
    // que se actualiza en las variables newHead y newEnd
    while (cur != pivot)
    {
        if ( trabajador_comparar(cur->pValor,pivot->pValor,campo_orden) ) // Un trabajador segun el campo de parametro
        {
            // El primer nodo que tiene un valor menor que el pivote se convierte en
            
            if ((*newHead) == NULL)
                (*newHead) = cur;
            
            prev = cur;
            cur = cur->prox;
        }
        else // If el nodo cur es mayor que el pivote
        {
            // Mueva el nodo cur al siguiente de la cola y cambie la cola
            if (prev)
                prev->prox = cur->prox;
            TNodoLista* tmp = cur->prox;
            cur->prox = NULL;
            tail->prox = cur;
            tail = cur;
            cur = tmp;
        }
    }
    
    // if los datos dinámicos son el elemento más pequeño de la lista actual,
    // pivote se convierte en la cabeza
    if ((*newHead) == NULL)
        (*newHead) = pivot;
    
    // Actualizar newEnd al último nodo actual
    (*newEnd) = tail;
    
    //Devuelve el nodo pivote
    return pivot;
}


//aquí la ordenación ocurre exclusivamente del nodo final
TNodoLista* quickSortRecur(TNodoLista *head, TNodoLista *end, int campo_orden)
{
    
    if (!head || head == end)
        return head;
    
    TNodoLista *newHead = NULL, *newEnd = NULL;
    
    // Particionar la lista, newHead y newEnd se actualizarán
    
    TNodoLista *pivot = partition(head, end, &newHead, &newEnd, campo_orden);
    
    // Si pivote es el elemento más pequeño, no es necesario recurrir para
    
    if (newHead != pivot)
    {
        // Establezca el nodo antes del nodo pivote como NULL
        TNodoLista* tmp = newHead;
        while (tmp->prox != pivot)
            tmp = tmp->prox;
        
        tmp->prox = NULL;
        
        // Recurrir para la lista antes de pivote
        newHead = quickSortRecur(newHead, tmp, campo_orden);
        
        // Cambie el siguiente del último nodo de la mitad izquierda para pivotar
        tmp = getTail(newHead);
        tmp->prox = pivot;
    }
    
    //Recurra para la lista después del elemento pivote
    
    
    pivot->prox = quickSortRecur(pivot->prox, newEnd, campo_orden);
    
    return newHead;
}

// La función principal para la ordenación rápida. Esta es una envoltura sobre recursiva
void quickSort(TNodoLista** headRef, int campo_orden)
{
    (*headRef) = quickSortRecur(*headRef, getTail(*headRef), campo_orden);
    return;
}

// function to find out middle element
TNodoLista* middle(TNodoLista* start, TNodoLista* last)
{
    if (start == NULL)
        return NULL;
    
    TNodoLista* slow = start;
    TNodoLista* fast = start->prox;
    
    while (fast != last)
    {
        fast = fast -> prox;
        if (fast != last)
        {
            slow = slow -> prox;
            fast = fast -> prox;
        }
    }
    
    return slow;
}

// Funcion para implementar la busqueda binaria en una lista enlazada
TNodoLista* binarySearch(TNodoLista* head, int id)
{
    TNodoLista* start = head;
    TNodoLista* last = NULL;
    
    do
    {
        // Find middle
        TNodoLista* mid = middle(start, last);
        
        // If middle is empty
        if (mid == NULL)
            return NULL;
        
        // If value is present at middle
        if (mid->pValor->id == id)
            return mid;
        
        // If value is more than mid
        else if (mid->pValor->id < id)
            start = mid -> prox;
        
        // If the value is less than mid.
        else
            last = mid;
        
    } while (last == NULL ||
             last != start);
    
    // value not present
    return NULL;
}

// Retorna un arreglo de enteros con los ids de las asignaturas segun las opciones disponibles
int* ingresar_asignaturas(const char** opciones, int* pCantidad) {
    int* asignaturas=NULL;
    int idAsignatura = 0;
    int i = 0;
    int cantidad=0;
    
    // Consulta la cantidad de asignaturas que imparte el profesor
    ingresar_entero_positivo("Especifique cuantas asignaturas imparte el profesor\n", &cantidad);
    if(cantidad>0 && cantidad<=25)
    {
        asignaturas = (int*) malloc((cantidad) * sizeof(int));
        for(i=0;i<cantidad;i++) {
            idAsignatura = 0;
            printf("Ingreso se asignatura N#%d\n",i+1);
            idAsignatura=ingresar_opcion(opciones);
            *(asignaturas+i) = idAsignatura;
        }
    }
    else if (cantidad>25)
    {
        printf("No es posible ingresar mas de 25 asignaturas por profesor.");
    }
    else
    {
        cantidad = 0;
    }
    
    *pCantidad = cantidad;
    
    return asignaturas;
    
}

// Despliega las asignaturas y todos los profesores que estan en ella si tipoTrabajador = 2 profesor planta, 3 = profesor honorarios
void profesores_por_asignatura(const char** enum_asignaturas, int tipoTrabajador, TNodoLista* lista) {
    TNodoLista* iterador=NULL;
    int i=1,j=0,sw=0;
    char** asignatura = (char**)enum_asignaturas;
    // Recorre las asignaturas
    while(*asignatura!=NULL)
    {
        sw=0;
        
        iterador = lista;
        
        while(iterador!=NULL) // recorre la lista de trabajadores
        {
            
            TTrabajador* trabajador = iterador->pValor;
            if(trabajador!=NULL)
            {
                if(trabajador->tipoTrabajador==tipoTrabajador)
                {
                    // chequea si el profesor tiene la asignatura
                    if(trabajador->cantidad_asignaturas>0) {
                        j=0;
                    
                        for(j=0;j<trabajador->cantidad_asignaturas;j++)
                        {
                            if(i== *(trabajador->id_asignaturas+j))
                            {
                                if(!sw)
                                {
                                    // Aun no se ha impreso el nombre de la asignatura?
                                    printf("ASIGNATURA N#%d : %s\n",i,*(asignatura));
                                }
                                printf("   PROF. ID: %d TIPO: %d, NOMBRE:%s\n",trabajador->id,trabajador->tipoTrabajador,trabajador->nombre_completo);
                                break;
                            }
                        }
                    }
                }
            }
            iterador=iterador->prox;
        }
        asignatura++; //
        i++;
    }
}
